package br.com.joao.treinamentoJava.service.impl;

import br.com.joao.treinamentoJava.entity.Usuario;
import br.com.joao.treinamentoJava.repository.UsuarioRepository;
import br.com.joao.treinamentoJava.service.UsuarioService;

import br.com.joao.treinamentoJava.vo.UsuarioVO;
import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;


/**
 * Classe que implementa o serviço de {@link Usuario}.
 *
 *
 */
@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	private UsuarioRepository usuariosRepository;


//	@Autowired
//	private AuthenticationFacade authenticationFacade;


	protected JpaRepository<Usuario, Long> getRepository() {

		return usuariosRepository;
	}

	@Autowired
	private PasswordEncoder encoder;




	@Override
	public UserDetails loadUserByUsername(String username) {

		Usuario usuario = usuariosRepository.findByEmail(username);
		if (usuario == null) {
			throw new UsernameNotFoundException(username);
		}
		return usuario;
	}



	public Usuario findByEmail(String email) {
		return usuariosRepository.findByEmail(email);
	}

	@Override
	public Optional<Usuario> findById(Long id) {
		return usuariosRepository.findById(id);
	}

	@Override
	public Usuario create(UsuarioVO vo) {
		Usuario usuario = new Usuario();
		usuario.setNome(vo.getNome());
		usuario.setSenha(encoder.encode(vo.getSenha()));
		usuario.setEmail(vo.getEmail());
		usuario.setStatus(true);
		usuario.setTipoPerfil(vo.getTipoPerfil());
		return usuariosRepository.save(usuario);
	}

	@Override
	public Usuario update(Usuario usuario, UsuarioVO vo) {
		usuario.setNome(vo.getNome());
		usuario.setSenha(encoder.encode(vo.getSenha()));
		usuario.setEmail(vo.getEmail());
		usuario.setStatus(vo.isStatus());
		return usuariosRepository.save(usuario);
	}

	@Override
	public Usuario inativarUsuario(Long id) {
		Usuario usuarioAtivo = new Usuario();

		if(usuariosRepository.existsById(id)){
			usuarioAtivo = usuariosRepository.findById(id).get();

			if (usuarioAtivo.getId().equals(id)){
				usuarioAtivo.setStatus(false);
				usuariosRepository.save(usuarioAtivo);
			}
		}
		return usuarioAtivo;
	}

	@Override
	public Usuario ativarUsuario(Long id) {
		Usuario usuarioInativo = new Usuario();

		if(usuariosRepository.existsById(id)){
			usuarioInativo = usuariosRepository.findById(id).get();

			if (usuarioInativo.getId().equals(id)){
				usuarioInativo.setStatus(true);
				usuariosRepository.save(usuarioInativo);
			}
		}
		return usuarioInativo;
	}
}
