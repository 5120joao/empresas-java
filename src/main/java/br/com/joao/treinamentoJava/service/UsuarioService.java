package br.com.joao.treinamentoJava.service;

import br.com.joao.treinamentoJava.entity.Usuario;
import br.com.joao.treinamentoJava.vo.UsuarioVO;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.Optional;

/**
 * Serviço do {@link Usuario}.
 *
 *
 */
public interface UsuarioService extends UserDetailsService {
    Optional<Usuario> findById(Long id);
    Usuario create(UsuarioVO vo);
    Usuario update(Usuario usuario, UsuarioVO vo);
    Usuario inativarUsuario(Long id);
    Usuario ativarUsuario(Long id);
}
