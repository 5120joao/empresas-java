package br.com.joao.treinamentoJava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Class to start the application.
 *
 */
@SpringBootApplication
public class WebservicesApplication {

	/**
	 * Initializing spring boot.
	 * 
	 * @param args
	 *            Args java.
	 */
	public static void main(String[] args) {

			SpringApplication.run(WebservicesApplication.class, args);
	}

}
