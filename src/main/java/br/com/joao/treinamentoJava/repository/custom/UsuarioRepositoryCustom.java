package br.com.joao.treinamentoJava.repository.custom;


import br.com.joao.treinamentoJava.entity.Usuario;
import br.com.joao.treinamentoJava.vo.request.FilterUsuarioVO;


import java.util.List;

public interface UsuarioRepositoryCustom {
    List<Usuario> findByFilter(FilterUsuarioVO filter);
    Long countByFilter(FilterUsuarioVO filter);
}
