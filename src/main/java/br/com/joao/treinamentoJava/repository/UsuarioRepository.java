package br.com.joao.treinamentoJava.repository;

import br.com.joao.treinamentoJava.entity.Usuario;
import br.com.joao.treinamentoJava.repository.custom.UsuarioRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * Interface repository do {@link Usuario}.
 *
 * @author joao
 *
 */
public interface UsuarioRepository extends JpaRepository<Usuario, Long>, UsuarioRepositoryCustom {

    Usuario findByEmail(String email);
}
