package br.com.joao.treinamentoJava.repository.custom.impl;

import br.com.joao.treinamentoJava.entity.Usuario;
import br.com.joao.treinamentoJava.repository.custom.UsuarioRepositoryCustom;
import br.com.joao.treinamentoJava.vo.UsuarioVO;
import br.com.joao.treinamentoJava.vo.request.FilterUsuarioVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class UsuarioRepositoryImpl implements UsuarioRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Usuario> findByFilter(FilterUsuarioVO filter) {
        StringBuilder hql = new StringBuilder();
        hql.append("SELECT c FROM usuario c WHERE c.tipo_Perfil=1");


        mountQuery(filter, hql);
        mountOrderBy(hql, filter.getOrder(), filter.getDirection());

        Query query = createQuery(filter, hql);
        if (filter.getPage() != null && filter.getPageSize() != null) {
            query.setFirstResult(filter.getPage() * filter.getPageSize());
            query.setMaxResults(filter.getPageSize());
        }

        return query.getResultList();
    }

    private void mountQuery(FilterUsuarioVO filter, StringBuilder hql) {
        if(filter!=null&&filter.getFilter()!=null) {
            UsuarioVO usuarioVO = filter.getFilter();
            if(StringUtils.isNotEmpty(usuarioVO.getNome())) {
                hql.append(" AND lower(c.nome) like :nome");
            }
            if(StringUtils.isNotEmpty(usuarioVO.getEmail())){
                hql.append(" AND lower(c.email) like :email ");
            }
        }
    }



    private Query createQuery(FilterUsuarioVO filter, StringBuilder hql) {
        Query query = entityManager.createQuery(hql.toString());
        if(filter!=null&&filter.getFilter()!=null) {
            UsuarioVO usuarioVO = filter.getFilter();
            if(StringUtils.isNotEmpty(usuarioVO.getNome())) {
                query.setParameter("nome", (usuarioVO.getNome().toLowerCase()));
            }
            if(StringUtils.isNotEmpty(usuarioVO.getEmail())){
                query.setParameter("email",(usuarioVO.getEmail().toLowerCase()));
            }
        }
        return query;

    }

    private void mountOrderBy(StringBuilder sql, String order, String direction) {
        if (order != null) {
            sql.append(" ORDER BY c.");
            sql.append(order);
            if (direction != null) {
                sql.append(" ");
                sql.append(direction);
            }
        }
    }

    @Override
    public Long countByFilter(FilterUsuarioVO filter) {
        StringBuilder hql = new StringBuilder();
        hql.append("SELECT count(c.id) FROM usuario c WHERE c.tipo_Perfil=1");
        mountQuery(filter, hql);
        Query query = createQuery(filter, hql);
        return (Long)query.getSingleResult();
    }
}
