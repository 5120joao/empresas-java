package br.com.joao.treinamentoJava.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.firewall.DefaultHttpFirewall;
import org.springframework.security.web.firewall.HttpFirewall;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		// @formatter:off
		http.authorizeRequests()
				.antMatchers("/v2/api-docs", "/swagger-resources/**", "/swagger-ui.html", "/resources/**", "/publico/**",
						"/webjars/**", "/oauth/**", "/error","/ws/**")
				.permitAll().and().authorizeRequests().anyRequest().authenticated().and().formLogin().loginPage("/login")
				.defaultSuccessUrl("/index").permitAll().and().logout().logoutUrl("/appLogout")
				.logoutSuccessUrl("/login").permitAll().and().exceptionHandling()
				.accessDeniedPage("/login?error=accessDenied").and().headers().frameOptions().disable().and().csrf()
				.disable();
		// @formatter:on
	}
	@Override
	public void configure(WebSecurity web) throws Exception {
	    
	    super.configure(web);
	    web.httpFirewall(allowUrlEncodedSlashHttpFirewall());
	
	}
	@Bean
    public HttpFirewall allowUrlEncodedSlashHttpFirewall() {
        DefaultHttpFirewall firewall = new DefaultHttpFirewall();
        
   
        firewall.setAllowUrlEncodedSlash(true);
        return firewall;
    }
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth, DaoAuthenticationProvider authenticationProvider)
			throws Exception {

		auth.authenticationProvider(authenticationProvider);

	}

	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {

		return super.authenticationManagerBean();
	}

	
}