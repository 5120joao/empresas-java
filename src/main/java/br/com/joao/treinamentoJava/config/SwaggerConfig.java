package br.com.joao.treinamentoJava.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.ApiKeyVehicle;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Classe para configurar o Swagger.
 * 
 * @author joao.oliveira
 *
 */
@Configuration
@EnableSwagger2
@Profile("!prod")
public class SwaggerConfig {

	/**
	 * Configuração inicial do swagger.
	 * 
	 * @return {@link Docket} com a configuração do swagger.
	 */
	@Bean
	public Docket newsApi() {

		//@formatter:off
		return new Docket(DocumentationType.SWAGGER_2)
				.ignoredParameterTypes(AuthenticationPrincipal.class)
				.apiInfo(apiInfo())
				.securitySchemes(Collections.singletonList(securitySchema()))
				.select()
				.apis(RequestHandlerSelectors.basePackage("br.com.marph.valemadmin.webservice.api"))
				.build();
		//@formatter:on
	}

	/**
	 * Configuração para utilizar o token de autenticação nas consultas do
	 * swagger.
	 * 
	 * @return Configurações de segurança.
	 */
	@Bean
	public SecurityConfiguration security() {

		return new SecurityConfiguration(null, null, null, null, "  ", ApiKeyVehicle.HEADER, "Bearer", null);
	}

	private ApiInfo apiInfo() {

		//@formatter:off
		return new ApiInfoBuilder()
				.title("Projeto Demo")
				.description("Projeto rest do Demo.")
				.contact(new Contact("Wesllei Ferreira", "http://www.marph.com.br", "wesllei.ferreira@marph.com.br"))
				.version("1.0")
				.build();
		//@formatter:on
	}

	private OAuth securitySchema() {

		List<AuthorizationScope> authorizationScopeList = new ArrayList<>();

		authorizationScopeList.add(new AuthorizationScope("web", "web"));
		ResourceOwnerPasswordCredentialsGrant resourceOwnerPasswordCredentialsGrant = new ResourceOwnerPasswordCredentialsGrant("http://localhost:8080/oauth/token");
		List<GrantType> grantTypes = Collections.singletonList(resourceOwnerPasswordCredentialsGrant);
		return new OAuth("securitySchemaOAuth2", authorizationScopeList, grantTypes);
	}

}
