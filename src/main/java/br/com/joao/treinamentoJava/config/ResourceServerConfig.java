package br.com.joao.treinamentoJava.config;

import br.com.joao.treinamentoJava.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

import java.text.Normalizer;
import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.Locale;


/**
 * Configuração do servidor de recurso.
 *
 * @author joao.oliveira
 *
 */
@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

	@Autowired
	private UsuarioService usuariosService;

	@Override
	public void configure(HttpSecurity http) throws Exception {

		//@formatter:off
		http.headers()
				.frameOptions().sameOrigin().and()
				.authorizeRequests()
				.antMatchers(HttpMethod.OPTIONS).permitAll().and().authorizeRequests()
				.antMatchers("/v2/api-docs", "/swagger-resources/**", "/swagger-ui.html", "/webjars/**", "/publico/**", "/ws/**").permitAll().and().authorizeRequests()
				.antMatchers("/**").authenticated();
		//@formatter:on
	}

	/**
	 * Configura a forma como o usuário vai ser recuperado.
	 *
	 * @param auth Builder contendo as configurações da autenticação.
	 */
	@Autowired
	public void globalUserDetails(final AuthenticationManagerBuilder auth) {

		auth.authenticationProvider(authenticationProvider());
	}

	/**
	 * Configura um serviço que irá fazer a consulta de usuário no sistema.
	 *
	 * @return Configuração do serviço e qual o método de senha utilizado.
	 */
	@Bean
	public DaoAuthenticationProvider authenticationProvider() {

		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(usuariosService);
		authProvider.setPasswordEncoder(encoder());
		return authProvider;
	}

	/**
	 * Configura uma forma global de como gerar o encoder da senha.
	 *
	 * @return retorna uma instancia do {@link BCryptPasswordEncoder}.
	 */
	@Bean
	public PasswordEncoder encoder() {

		return new BCryptPasswordEncoder(11);
	}

	public static void main(String[] args) {
		System.out.println(new BCryptPasswordEncoder().encode("webclientsupersecret"));
		System.out.println(new BCryptPasswordEncoder().encode("marph1234"));
		
		LocalDate date = LocalDate.now();
		System.out.println(date.getMonth().getDisplayName(TextStyle.SHORT, new Locale("pt")));
		
		String s = "Wesllei Gustavo ferreira";
		System.out.println(s.replaceAll("^\\s*([a-zA-Z]).*\\s+([a-zA-Z])\\S+$", "$1$2").toUpperCase());

		
		
	}

	public static String removerAcentos(String str) {
		return Normalizer.normalize(str, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
	}
}