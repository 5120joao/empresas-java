package br.com.joao.treinamentoJava.entity;

import br.com.joao.treinamentoJava.enums.TipoPerfilEnum;
import br.com.joao.treinamentoJava.vo.UsuarioVO;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@Entity
@Table(name = "usuario")
@Data
public class Usuario implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "email", nullable = false)
    private  String email;

    @Column(name = "nome", nullable = false)
    private String nome;

    @Column(name = "senha", nullable = false)
    private String senha;

    @Column(name = "tipo_perfil")
    @Enumerated(EnumType.ORDINAL)
    private TipoPerfilEnum tipoPerfil;

    @Column(name = "status", nullable = false)
    private boolean status;


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {

        List<GrantedAuthority> authorities = new ArrayList<>();
        if (getTipoPerfil() != null) {
            authorities.add(new SimpleGrantedAuthority(getTipoPerfil().getDescription()));
        }
        return authorities;
    }
    @Override
    public String getPassword() {

        return senha;
    }

    @Override
    public String getUsername() {

        return email;
    }

    @Override
    public boolean isAccountNonExpired() {

        return true;
    }

    @Override
    public boolean isAccountNonLocked() {

        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {

        return true;
    }

    @Override
    public boolean isEnabled() {

        return status;
    }



    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Usuario other = (Usuario) obj;
        if (getId() == null) {
            if (other.getId() != null)
                return false;
        } else if (!getId().equals(other.getId()))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        return result;
    }

    public Usuario (){

    }


    public UsuarioVO toVO() {
        UsuarioVO vo =new UsuarioVO();
        vo.setId(getId());
        vo.setNome(getNome());
        vo.setTipoPerfil(getTipoPerfil());
        vo.setEmail(getEmail());
        vo.setStatus(isStatus());

        return vo;
    }

    public UsuarioVO toSmallVO() {
        UsuarioVO vo =new UsuarioVO();
        vo.setId(getId());
        vo.setNome(getNome());
        vo.setEmail(getEmail());
        vo.setTipoPerfil(getTipoPerfil());
        return vo;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
