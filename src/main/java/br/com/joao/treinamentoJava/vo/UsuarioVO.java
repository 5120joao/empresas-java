package br.com.joao.treinamentoJava.vo;

import br.com.joao.treinamentoJava.entity.Usuario;
import br.com.joao.treinamentoJava.enums.TipoPerfilEnum;
import lombok.Data;

import java.io.Serializable;

@Data
public class UsuarioVO implements Serializable {
    private Long id;

    private  String email;

    private String senha;

    private String nome;

    private TipoPerfilEnum tipoPerfil;

    private boolean status;

    public UsuarioVO(){

    }

   public Usuario toEntity() {
       Usuario usuario = new Usuario();
       usuario.setId(getId());
       return usuario;
   }

}
