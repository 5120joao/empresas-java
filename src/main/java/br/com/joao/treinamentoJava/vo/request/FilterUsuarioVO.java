package br.com.joao.treinamentoJava.vo.request;

import br.com.joao.treinamentoJava.vo.UsuarioVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class FilterUsuarioVO extends FilterRequestVO<UsuarioVO> {
}
