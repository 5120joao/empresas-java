package br.com.joao.treinamentoJava.vo.request;

import java.io.Serializable;
import java.util.List;

public class FilterRequestVO<E extends Serializable> extends RequestVO implements Serializable {
    private static final Integer PAGE_SIZE = 20;
    private static final long serialVersionUID = 1L;
    private Integer page;
    private Integer pageSize;
    private String order;
    private String direction;
    private E filter;
    private List<String> fields;
    private List<String> labels;

    public FilterRequestVO() {
        this(0, 10, (String)null, (String)null);
    }

    public FilterRequestVO(Integer page, Integer pageSize, String order, String direction) {
        this.page = page == null ? 0 : page;
        this.pageSize = pageSize == null ? PAGE_SIZE : pageSize;
        this.order = order;
        this.direction = direction == null ? "ASC" : direction;
    }

    public Integer getPage() {
        return this.page;
    }

    public Integer getPageSize() {
        return this.pageSize;
    }

    public String getOrder() {
        return this.order;
    }

    public String getDirection() {
        return this.direction;
    }

    public E getFilter() {
        return this.filter;
    }

    public List<String> getFields() {
        return this.fields;
    }

    public List<String> getLabels() {
        return this.labels;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public void setFilter(E filter) {
        this.filter = filter;
    }

    public void setFields(List<String> fields) {
        this.fields = fields;
    }

    public void setLabels(List<String> labels) {
        this.labels = labels;
    }

    public String toString() {
        return "FilterRequestVO(page=" + this.getPage() + ", pageSize=" + this.getPageSize() + ", order=" + this.getOrder() + ", direction=" + this.getDirection() + ", filter=" + this.getFilter() + ", fields=" + this.getFields() + ", labels=" + this.getLabels() + ")";
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof FilterRequestVO)) {
            return false;
        } else {
            FilterRequestVO<?> other = (FilterRequestVO)o;
            if (!other.canEqual(this)) {
                return false;
            } else if (super.equals(o)) {
                return false;
            } else {
                label97: {
                    Object this$page = this.getPage();
                    Object other$page = other.getPage();
                    if (this$page == null) {
                        if (other$page == null) {
                            break label97;
                        }
                    } else if (this$page.equals(other$page)) {
                        break label97;
                    }

                    return false;
                }

                Object this$pageSize = this.getPageSize();
                Object other$pageSize = other.getPageSize();
                if (this$pageSize == null) {
                    if (other$pageSize != null) {
                        return false;
                    }
                } else if (!this$pageSize.equals(other$pageSize)) {
                    return false;
                }

                Object this$order = this.getOrder();
                Object other$order = other.getOrder();
                if (this$order == null) {
                    if (other$order != null) {
                        return false;
                    }
                } else if (!this$order.equals(other$order)) {
                    return false;
                }

                label76: {
                    Object this$direction = this.getDirection();
                    Object other$direction = other.getDirection();
                    if (this$direction == null) {
                        if (other$direction == null) {
                            break label76;
                        }
                    } else if (this$direction.equals(other$direction)) {
                        break label76;
                    }

                    return false;
                }

                Object this$filter = this.getFilter();
                Object other$filter = other.getFilter();
                if (this$filter == null) {
                    if (other$filter != null) {
                        return false;
                    }
                } else if (!this$filter.equals(other$filter)) {
                    return false;
                }

                Object this$fields = this.getFields();
                Object other$fields = other.getFields();
                if (this$fields == null) {
                    if (other$fields != null) {
                        return false;
                    }
                } else if (!this$fields.equals(other$fields)) {
                    return false;
                }

                Object this$labels = this.getLabels();
                Object other$labels = other.getLabels();
                if (this$labels == null) {
                    if (other$labels != null) {
                        return false;
                    }
                } else if (!this$labels.equals(other$labels)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(Object other) {
        return other instanceof FilterRequestVO;
    }

    public int hashCode() {
        boolean PRIME = true;
        int result = super.hashCode();
        Object $page = this.getPage();
        result = result * 59 + ($page == null ? 43 : $page.hashCode());
        Object $pageSize = this.getPageSize();
        result = result * 59 + ($pageSize == null ? 43 : $pageSize.hashCode());
        Object $order = this.getOrder();
        result = result * 59 + ($order == null ? 43 : $order.hashCode());
        Object $direction = this.getDirection();
        result = result * 59 + ($direction == null ? 43 : $direction.hashCode());
        Object $filter = this.getFilter();
        result = result * 59 + ($filter == null ? 43 : $filter.hashCode());
        Object $fields = this.getFields();
        result = result * 59 + ($fields == null ? 43 : $fields.hashCode());
        Object $labels = this.getLabels();
        result = result * 59 + ($labels == null ? 43 : $labels.hashCode());
        return result;
    }
}
