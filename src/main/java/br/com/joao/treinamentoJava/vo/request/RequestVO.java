package br.com.joao.treinamentoJava.vo.request;

import java.io.Serializable;

public abstract class RequestVO implements Serializable {
    private static final long serialVersionUID = 1L;

    protected RequestVO() {
    }
}
