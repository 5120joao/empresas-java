package br.com.joao.treinamentoJava.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;

import java.util.LinkedHashMap;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@Getter
public enum TipoPerfilEnum {
	ADMINISTRADOR ( "Administrador"),CLIENTE( "Cliente");
	/*
		ADMINISTRADOR 0
		CLIENTE 1


	 */
	private String description;
	private String name;
	private Integer codigo;


	TipoPerfilEnum(String description) {
		this.description = description;
		this.name = name();
	}

	public String getDescription() {
		return this.description;
	}

	@JsonCreator
	public static TipoPerfilEnum fromString(Object string) {

		return TipoPerfilEnum.valueOf((String) ((LinkedHashMap) string).get("name"));
	}

}
