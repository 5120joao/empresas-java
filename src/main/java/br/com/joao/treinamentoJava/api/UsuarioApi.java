package br.com.joao.treinamentoJava.api;

import br.com.joao.treinamentoJava.entity.Usuario;
import br.com.joao.treinamentoJava.service.UsuarioService;
import br.com.joao.treinamentoJava.vo.UsuarioVO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;


import java.util.Optional;


@RestController
@RequestMapping("/publico/usuario")
public class UsuarioApi{


    @Autowired
    private UsuarioService usuarioService;

    @ApiOperation("Cria objeto")
    @PostMapping(
            path = {"/create"},
            consumes = {"application/json"}
    )
    public Usuario create(@RequestBody UsuarioVO vo) {
        return usuarioService.create(vo);
    }

    @ApiOperation("Atualiza objeto")
    @PutMapping(
            path = {"/update/{id}"},
            consumes = {"application/json"}
    )
    public Usuario update(@RequestBody UsuarioVO vo, @PathVariable(name = "id") Long id) {
        Optional<Usuario> usuario = usuarioService.findById(id);
        if(usuario.isPresent()) {
            return usuarioService.update((Usuario)usuario.get(), vo);
        }else {
            return null;
        }
    }

    @GetMapping("/inativar/{id}")
    public UsuarioVO inativarUsuario(@PathVariable(value = "id") Long id){
        UsuarioVO usuarioAtivo = (usuarioService.inativarUsuario(id)).toVO();
        return usuarioAtivo;
    }

    @GetMapping("/ativar/{id}")
    public UsuarioVO ativarUsuario(@PathVariable(value = "id") Long id){
        UsuarioVO usuarioInativo = (usuarioService.ativarUsuario(id)).toVO();
        return usuarioInativo;
    }


}